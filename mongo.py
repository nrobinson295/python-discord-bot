
from pymongo import MongoClient
import os
from dotenv import load_dotenv


load_dotenv()
mongodb = os.environ["MONGOATLAS"]
cluster = MongoClient(mongodb)
db = cluster["discord"]
collection = db["users"]


# possible idea to create slash command for trading materials
