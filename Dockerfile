FROM python:3.9-alpine
WORKDIR /build
COPY requirements.txt /build/requirements.txt
COPY main.py /build/main.py
COPY mongo.py /build/mongo.py
RUN apk add build-base
RUN python3.9 -m pip install -r requirements.txt
CMD python3.9 main.py