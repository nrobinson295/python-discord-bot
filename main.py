
import os
from bson.objectid import ObjectId
import discord
from discord_slash import SlashCommand
from dotenv import load_dotenv
from discord_slash.utils.manage_commands import create_choice, create_option
import mongo
from pymongo import ReturnDocument


# https://discord.com/api/oauth2/authorize?client_id=873067655354015754&scope=bot%20applications.commands&permissions=311385345088

load_dotenv()
client = discord.Client(intents=discord.Intents.all())
slash = SlashCommand(client, sync_commands=True)
guild_id = []
token = os.environ['TOKEN']


def setup():
    g = os.environ['GUILDID']
    if g != None and g != "":
        guild_id = [int(g)]


setup()


@client.event
async def on_ready():
    print("Ready!")


@slash.slash(name="war",
             description="Player Information",
             guild_ids=guild_id,
             options=[
                 create_option(
                     name="ign",
                     description="State your ingame name",
                     option_type=3,
                     required=True
                 ),
                 create_option(
                     name="level",
                     description="Your characters level",
                     option_type=4,
                     required=True
                 ),
                 create_option(
                     name="build",
                     description="pick your build from the options",
                     option_type=3,
                     required=True,
                     choices=[
                         create_choice(
                             name="Tank - Sword&Board with Hammer/Axe",
                             value="Tank"
                         ),
                         create_choice(
                             name="Mage - Firestaff",
                             value="Mage"
                         ),
                         create_choice(
                             name="Healer - Lifestaff",
                             value="Healer"
                         ),
                         create_choice(
                             name="Ranged DPS - Bow, Musket",
                             value="Ranged DPS"
                         ),
                         create_choice(
                             name="Bruiser DPS - Axe/Hammer, Hammer/No Sword, Axe/No Sword",
                             value="Bruiser DPS"
                         ),
                         create_choice(
                             name="Melee DPS - Hatchet, Spear, Raipier",
                             value="Melee DPS"
                         )
                     ]
                 ),
                 create_option(
                     name="armor_class",
                     description="Select your armor type",
                     option_type=3,
                     required=True,
                     choices=[
                         create_choice(
                             name="Light",
                             value="Light"
                         ),
                         create_choice(
                             name="Medium",
                             value="Medium"
                         ),
                         create_choice(
                             name="Heavy",
                             value="Heavy"
                         )
                     ]
                 ),
                 create_option(
                     name="company",
                     description="List your company name",
                     option_type=3,
                     required=True,
                 ),
                 create_option(
                     name="mastery",
                     description="State your Weapon and it's Mastery Level",
                     option_type=3,
                     required=True
                 ),
             ])
# slash "war" function
async def war(ctx, ign: str, level: int, build: int, armor_class: str, company: str, mastery: str):
    # jumps to getWar function to try and find a userid match in the database
    warInfo = await getWar(ctx.author_id)
    # gathering users new input for new creation or update a users information
    warInfo["userid"] = ctx.author_id
    warInfo["ign"] = ign
    warInfo["level"] = level
    warInfo["build"] = build
    warInfo["armor_class"] = armor_class
    warInfo["company"] = company
    warInfo["mastery"] = mastery
    await updateWar(warInfo)
    # calling embed function and passing users war information
    await createEmbed(ctx, warInfo)


async def getWar(userid):
    # checking database for object matching the userid
    warInfo = mongo.collection.find_one({"userid": userid})
    # if None found return new object
    if warInfo == None:
        return{"_id": ObjectId()}
    return warInfo


# userid found now update information


async def updateWar(warInfo):
    warInfo = mongo.collection.find_one_and_replace(
        {"_id": warInfo["_id"]}, warInfo, upsert=True, return_document=ReturnDocument.AFTER)
    return warInfo


async def createEmbed(ctx, warInfo):
    # creates the embed displayed on discord channel
    embed = discord.Embed(title="War Application",
                          description="Player information", color=getColor(warInfo["build"]))
    embed.add_field(name="IGN:", value=warInfo["ign"], inline=True)
    embed.add_field(name="Level:", value=warInfo["level"], inline=True)
    embed.add_field(name="Build:", value=warInfo["build"], inline=True)
    embed.add_field(name="Armor Class:",
                    value=warInfo["armor_class"], inline=True)
    embed.add_field(name="Company:", value=warInfo["company"], inline=True)
    embed.add_field(name="Weapon and Mastery Level:",
                    value=warInfo["mastery"], inline=False)
    await ctx.send(embed=embed)


# handles color code based on selected build
def getColor(build):
    if build == "Tank":
        return 0xf6ff33
    if build == "Mage":
        return 0x2431ff
    if build == "Healer":
        return 0xff33e0
    if build == "Ranged DPS":
        return 0x33fbe6
    if build == "Bruiser DPS":
        return 0xff8e07
    if build == "Melee DPS":
        return 0x37f31a


client.run(token)
